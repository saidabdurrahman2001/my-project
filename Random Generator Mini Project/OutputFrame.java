import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

public class OutputFrame extends JFrame {
    
    public OutputFrame(String nama){
        ArrayList<String> kumpulanNama = new ArrayList<>();
        for (String i : nama.split(",")){
            kumpulanNama.add(i);
        }
        int jumlahOrang = kumpulanNama.size();
        
        setVisible(true);
        setSize(450,370);
        setLocationRelativeTo(null);
        setTitle("Random Generator");
        // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setBackground(new Color(255, 253, 208));
        panel.setLayout(new GridLayout(kumpulanNama.size(), 0,100,0));
        add(panel);

        for(int i = 0; i<jumlahOrang; i++){
            int index = (int) (Math.random() * kumpulanNama.size());
            JLabel temp = new JLabel(kumpulanNama.get(index));
            temp.setHorizontalAlignment(JLabel.CENTER);
            panel.add(temp);
            kumpulanNama.remove(index);
        }

        panel.revalidate(); panel.repaint();


    }

}
