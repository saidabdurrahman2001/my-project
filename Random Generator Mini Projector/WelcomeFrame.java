import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class WelcomeFrame extends JFrame {
    JLabel judul;
    JLabel nama;
    JTextField kumpulanNama;
    JButton generateRandom;
    
    public WelcomeFrame(){
        setVisible(true);
        setSize(450,370);
        setLocationRelativeTo(null);
        setTitle("Random Generator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setBackground(new Color(255, 253, 208));
        panel.setLayout(null);
        add(panel);

        judul = new JLabel("WELCOME TO RANDOM GENERATOR");
        judul.setBounds(110,30,500,20);
        panel.add(judul);

        nama = new JLabel("Masukkan nama-nama dan pisahkan dengan tanda ','");
        nama.setBounds(60,100,500,20);
        panel.add(nama);

        kumpulanNama = new JTextField();
        kumpulanNama.setBounds(30, 150, 380, 20);
        panel.add(kumpulanNama);

        generateRandom = new JButton("Generate Random!");
        generateRandom.setBounds(125,200,200,20);
        generateRandom.addActionListener(new GenerateButton());
        panel.add(generateRandom);

        panel.revalidate(); panel.repaint();
    }

    class GenerateButton implements ActionListener {
        
        public void actionPerformed(ActionEvent e){
            new OutputFrame(kumpulanNama.getText());
            // dispose();
        }


    }

}
